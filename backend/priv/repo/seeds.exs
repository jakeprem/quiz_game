# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Backend.Repo.insert!(%Backend.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Backend.{Repo, Question, QuestionData}
alias Backend.Data.Quiz

Repo.insert!(%Quiz{
  title: "Can you cross this bridge?",
  questions: [
    %Question{
      data: %QuestionData{
        question_type: "text",
        question_text: "What is your name?",
      }
    },
    %Question{
      data: %QuestionData{
        question_type: "text",
        question_text: "What is your quest?",
      }
    },
    %Question{
      data: %QuestionData{
        question_type: "multiple_choice",
        question_text: "What is your favorite color?",
        question_answers: ["Red", "Blue", "Green", "Yellow"],
      }
    },
  ]
})

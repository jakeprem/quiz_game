defmodule Backend.Repo.Migrations.CreateQuestions do
  use Ecto.Migration

  def change do
    create table(:questions) do
      add :quiz_id, references(:quizzes, on_delete: :nothing)
      add :data, :map

      timestamps()
    end

    create index(:questions, [:quiz_id])
  end
end

defmodule Backend.GameChannel do
  use Phoenix.Channel

  alias Game.QuizGame

  # Lobby --------------------------------------
  def join("game:" <> game_id, %{"name" => name}, socket) do
    {:ok, game_pid} = Game.GameServer.lookup(game_id)
    ref = Process.monitor(game_pid)

    socket = socket
      |> assign(:game_id, game_id)
      |> assign(:game_pid, game_pid)
      |> assign(:player_name, name)
      |> assign(:ref, ref)

    case QuizGame.join(game_pid, name) do
      {:ok, {title, players}} -> {:ok, %{game_id: game_id, players: players, title: title}, socket}
      {:error, reason} -> {:error, %{reason: reason}}
    end
  end

  def handle_in("start_game", _msg, socket) do
    {:ok, resp} = QuizGame.start_game(socket.assigns.game_pid)

    {:reply, {:ok, %{resp: resp}}, socket}
  end
  # --------------------------------------------

  # Question -----------------------------------
  def handle_in("answer", msg, socket) do
    {:reply, {:ok, %{resp: ""}}, socket}
  end
  # --------------------------------------------
end

defmodule Backend.RoomChannel do
  use Phoenix.Channel

  def join("room:lobby", _params, socket) do
    {:ok, socket}
  end

  def join("room:" <> _subtopic, _params, _socket) do
    {:error, %{reason: "Room does not exist"}}
  end

  def handle_in("start_game", _msg, socket) do
    {:ok, game_id, _game_pid} = Game.GameServer.start_game()

    {:reply, {:ok, %{game_id: game_id}}, socket}
  end
end

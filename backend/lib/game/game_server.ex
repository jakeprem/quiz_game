defmodule Game.GameServer do
  use GenServer

  alias Game.{
    QuizGame
  }

  ################
  ## Client API ##
  ################
  def start_link([]) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def start_game do
    GenServer.call(__MODULE__, :start_game)
  end

  def lookup(id) do
    GenServer.call(__MODULE__, {:lookup, id})
  end

  ######################
  ## Server Callbacks ##
  ######################
  def init(:ok) do
    games = %{}
    refs = %{}
    {:ok, {games, refs}}
  end

  def handle_call(:start_game, _from, {games, refs}) do
    new_id = Nanoid.generate(4, "ABCDEFGHIJKLMNOPQRSTUVWXYZ")

    quiz = Backend.Data.get_quiz!(2) |> Backend.Repo.preload(:questions)

    {:ok, pid} = DynamicSupervisor.start_child(Game.GameSupervisor, QuizGame.child_spec(%{quiz: quiz, game_id: new_id}))
    ref = Process.monitor(pid)
    refs = Map.put(refs, ref, new_id)
    games = Map.put(games, new_id, pid)
    {:reply, {:ok, new_id, pid}, {games, refs}}
  end

  def handle_call({:lookup, id}, _from, {games, _refs} = state) do
    {:reply, Map.fetch(games, id), state}
  end

  def handle_info({:DOWN, ref, :process, _pid, _reason}, {games, refs}) do
    {game, refs} = Map.pop(refs, ref)
    games = Map.delete(games, game)
    {:noreply, {games, refs}}
  end

  def handle_info(_msg, state) do
    {:noreply, state}
  end

  #######################
  ## Private Functions ##
  #######################
end

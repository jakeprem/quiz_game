defmodule Game.QuizGame do
  @behaviour :gen_statem

  # alias Game.QuizGame
  alias BackendWeb.Endpoint

  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: { __MODULE__, :start_link, [opts]},
      restart: :temporary,
      shutdown: 5000,
      type: :worker
    }
  end

  ################
  ## Client API ##
  ################
  def start_link(%{quiz: quiz, game_id: game_id}) do
    :gen_statem.start_link(__MODULE__, %{quiz: quiz, game_id: game_id}, [])
  end

  def join(pid, player_name) do
    :gen_statem.call(pid, {:join, player_name})
  end
  def start_game(pid) do
    :gen_statem.call(pid, :start_game)
  end

  def get_count(pid) do
    :gen_statem.call(pid, :get_count)
  end


  ######################
  ## Server Callbacks ##
  ######################
  @impl :gen_statem
  def callback_mode() do
    [:handle_event_function, :state_enter]
  end

  # -- Init -------------------------------------------------
  @impl :gen_statem
  def init(%{quiz: quiz, game_id: game_id}) do
    {:ok, :lobby, %{questions: quiz.questions, players: [], current_question: "", current_question_answers: [], game_id: game_id, title: quiz.title}}
  end
  # ----------------------------------------------------------

  # -- Lobby -------------------------------------------------
  @impl :gen_statem
  def handle_event(:enter, _, :lobby, %{game_id: game_id, title: title, players: players}) do
    broadcast(game_id, "update_state:lobby", %{title: title, game_id: game_id, players: players})
    {:keep_state_and_data, []}
  end
  def handle_event({:call, from}, {:join, player_name}, :lobby, %{players: players, title: title, game_id: game_id} = data) do
    new_players = [player_name | players]

    broadcast(game_id, "update_state:lobby", %{title: title, game_id: game_id, players: players})
    {:keep_state, %{data | players: new_players}, just_reply(from, {title, new_players})}
  end
  def handle_event({:call, from}, :start_game, :lobby, data) do
    {:next_state, :question, data, just_reply(from, "Game was started.")}
  end
  def handle_event(_, _, :lobby, data) do
    IO.puts("LOBBY: You can't do that here. #{inspect data}")
  end
  # ----------------------------------------------------------

  # -- Show Question -----------------------------------------
  def handle_event(:enter, _, :question, %{questions: [question | rest], game_id: game_id} = data) do
    updated_data = %{data |
      current_question: question,
      questions: rest
    }

    broadcast(game_id, "update_state:question", %{type: question.data.question_type, text: question.data.question_text, choices: question.data.question_answers})
    {:keep_state, updated_data}
  end
  def handle_event({:call, from}, {:answer, player_name, answer}, :question, %{players: players, current_question_answers: current_question_answers} = data) do
      updated_data = %{data | current_question_answers: [{player_name, answer}|current_question_answers]}

      if Enum.count(players) == Enum.count(current_question_answers) do
        {:next_state, :answer, updated_data, just_reply(from, "ok")}
      else
        {:keep_state, updated_data}
      end
  end
  # ----------------------------------------------------------

  # -- Show Answer -------------------------------------------
  # ----------------------------------------------------------

  # -- Show Score --------------------------------------------
  # ----------------------------------------------------------

  # -- End State ---------------------------------------------
  # ----------------------------------------------------------

  # @impl :gen_statem
  # def handle_event(:enter, _, :lobby, _) do
  #   IO.puts "Entered lobby"
  #   {:keep_state_and_data, []}
  # end
  # def handle_event({:call, from}, {:join, player_name}, :lobby, %{players: players} =data) do
  #   IO.puts "PLAYER JOINED: #{player_name}"
  #   {:keep_state, %{data | players: [player_name|players]}, [{:reply, from, {:ok, players}}]}
  # end
  # def handle_event({:call, from}, :start_game, :lobby, data) do
  #   IO.puts("start game")
  #   {:next_state, :accept_responses, data, [{:reply, from, data}]}
  # end

  # def handle_event(:enter, _, :accept_responses, data) do
  #   IO.puts(inspect(data))
  #   {:keep_state_and_data, []}
  # end

  # def handle_event({:call, from}, :get_count, state, data) do
  #   {:next_state, state, data, [{:reply, from, data}]}
  # end

  def handle_event({:call, from}, {:join, _}, _state, _data) do
    {:keep_state_and_data, [{:reply, from, {:error, "Game isn't accepting new players"}}]}
  end
  def handle_event(event, content, state, _data) do
    IO.puts("ERROR: #{inspect event}")
    IO.puts("ERROR: Cannot do #{inspect content} in state #{state}")
    {:keep_state_and_data, []}
  end

  #######################
  ## Private Functions ##
  #######################
  def get_questions do
    ["What is your name?", "What is your quest?", "What is your favorite color?"]
  end

  defp reply_to_caller(from, data) do
    {:reply, from, {:ok, data}}
  end

  defp just_reply(from, data) do
    [reply_to_caller(from, data)]
  end

  defp broadcast(game_id, event, msg) do
    Endpoint.broadcast!("game:"<>game_id, event, msg)
  end
end

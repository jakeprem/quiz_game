defmodule Game.Application do
  use Application

  def start(_types, _args) do
    Game.Supervisor.start_link(name: Game.Supervisor)
  end
end

defmodule Backend.Data.Quiz do
  use Ecto.Schema
  import Ecto.Changeset


  schema "quizzes" do
    field :title, :string
    has_many :questions, Backend.Question

    timestamps()
  end

  @doc false
  def changeset(quiz, attrs) do
    quiz
    |> cast(attrs, [:title])
    |> validate_required([:title])
  end
end

defmodule Backend.Question do
  use Ecto.Schema
  import Ecto.Changeset


  schema "questions" do
    belongs_to :quiz, Backend.Data.Quiz

    embeds_one :data, Backend.QuestionData

    timestamps()
  end

  @doc false
  def changeset(schema, attrs \\ %{}) do
    schema
    |> cast(attrs, [])
    |> assoc_constraint(:quiz)
  end
end

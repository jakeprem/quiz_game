defmodule Backend.QuestionData do
  use Ecto.Schema
  import Ecto.Changeset
  alias Backend.QuestionData

  embedded_schema do
    field :question_type, :string
    field :question_text, :string
    field :question_answers, {:array, :string}
  end

  @doc false
  def changeset(%QuestionData{} = question_data, attrs) do
    question_data
    |> cast(attrs, [:question_data])
    |> validate_required([:question_data])
  end
end

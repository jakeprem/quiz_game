# QuizGame States
## 1. Lobby
### Events
  * Enter
    * Set state (possibly could be done in init)

  * Join
    * Add player to players
  * Start Game
    * Pop question => Question
## 2. Question
### Events
  * Enter
  * Answer
    * Everyone answered => Show Answer
  * Timeout 
    * => Show Answer
## 3. Show Answer
### Events
  * Enter
  * Timeout
    * => Show Score
## 4. Show Score
### Events
  * Enter
  * Timeout
    * Questions left => Question
    * All questions answered => End State/Final Screen
## 5. End State
### Events
  * Enter
  * Join
  * Start New
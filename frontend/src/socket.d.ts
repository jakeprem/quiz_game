import Vue from 'vue'
import { Socket } from 'phoenix'

declare module 'vue/types/vue' {
  interface Vue {
    $socket: Socket;
  }
}
import { Socket } from "phoenix";

const socketUrl = 'ws://localhost:4000/socket'
const socket = new Socket(socketUrl)
socket.connect()

export default socket
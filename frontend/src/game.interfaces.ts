export interface LobbyState {
  title: string
  players: string[]
  game_id: string
}

export interface QuestionState {
  type: string
  text: string
  choices: string[]
}

export interface AnswerState {
  answer: string[]
  choices: []
}

export interface PlayerScore {
  player: string
  score: number
}

export interface ScoreState {
  scores: PlayerScore[]
}